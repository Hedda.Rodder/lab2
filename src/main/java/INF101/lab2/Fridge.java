package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size = 20;

    ArrayList<FridgeItem> items = new ArrayList<>();


    @Override
    public int nItemsInFridge() {
        return items.size();
    }

    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (totalSize() > nItemsInFridge()) {
            items.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (nItemsInFridge() == 0) {
            throw new java.util.NoSuchElementException();
        } else {
            items.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        items.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<>();
        ArrayList<FridgeItem> goodItems = new ArrayList<>();
        for (FridgeItem i : items) {
            if (i.hasExpired()) {
                expiredItems.add(i);
            } else {
                goodItems.add(i);
            }
        }
        items.removeAll(expiredItems);
        return expiredItems;
    }
}
